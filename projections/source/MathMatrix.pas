//***************************************************************************
//*    +----------------------------------------------------------------+
//*    |        ��������, ������������� 0800, ����������� 0830          |
//*    |     ----------------------------------------------------       |
//*    | �������� �������� ���������������� ����������.                 |
//*    | ������ � 2009 ����. ��� ������������� � ����������� ���������� |
//*    | ��������� "Copyright 2009 ��������."                           |
//*    +----------------------------------------------------------------+
//*
//*  Project:   ���
//*
//*  File Name: MathMatrix.pas
//*
//*  Author:    ������ �. �.
//*
//*  Decription: ���������� ��������� ����������
//*
//*
//*  $Revision: 13 $
//*
//*  $Date:: 2009-07-17 12:19:14#$
//*
//*  ������� ���������
//*  �����          ����       ���       �������� ���������
//*  ------------  --------  -------  ---------------------------------
//*  ������ �. �.  01.04.09           ������ ������ �����
//*
//*
//***************************************************************************

unit MathMatrix;

interface

uses
  SysUtils, Math;

type
  Float = extended;

  Tv = array of Float;
  Tm = array of Tv;

function Summ(A, B: Tm; m, n: integer): Tm;
function multiply(A: Tm; m, n: integer; B: Tm; l, k: integer): Tm;
function min(A: Tm; m, n: integer): Tm;
function tr(A: Tm; m, n: integer): Tm;
function det(B: Tm; m: integer): Float;
function Back(A: Tm; n: integer): Tm;

implementation

function Summ(A, B: Tm; m, n: integer): Tm;
var
  i, j: integer;
begin
  SetLength(A, m + 1, n + 1);
  SetLength(result, m + 1, n + 1);
  for i := 1 to m do
    for j := 1 to n do
      result[i, j] := A[i, j] + B[i, j];
end;

function multiply(A: Tm; m, n: integer; B: Tm; l, k: integer): Tm;
var
  i, j, p: integer;
  S: Float;
begin
  SetLength(A, m + 1, n + 1);
  SetLength(B, l + 1, k + 1);
  SetLength(result, m + 1, k + 1);
  for i := 1 to m do
  begin
    for p := 1 to k do
    begin
      S := 0;
      for j := 1 to n do
        S := S + A[i, j] * B[j, p];
      result[i, p] := S;
    end;
  end;
end;

function min(A: Tm; m, n: integer): Tm;
var
  i, j: integer;
begin
  SetLength(A, m + 1, n + 1);
  SetLength(result, m + 1, n + 1);
  for i := 1 to m do
    for j := 1 to n do
      result[i, j] := -1 * A[i, j];
end;

function tr(A: Tm; m, n: integer): Tm;
var
  i, j: integer;
begin
  SetLength(A, m + 1, n + 1);
  SetLength(result, n + 1, m + 1);
  for i := 1 to m do
    for j := 1 to n do
      result[j, i] := A[i, j];
end;

function det(B: Tm; m: integer): Float;
var
  i, j, n, o: integer;
  A: Tv;
  B2: Tm;
  k, S: Float;
begin
  SetLength(B, m + 2, m + 2);
  SetLength(B2, m + 1, m + 1);
  SetLength(A, m + 1);
  result := 1;
  o := 1;
  if o = 1 then
    k := 0;
  if B[1, 1] = 0 then
  begin
    o := 0;
    for i := 2 to m do
      if B[i, 1] <> 0 then
        o := i;
    for i := 1 to m do
      A[i] := B[1, i];
    for i := 1 to m do
      B[1, i] := B[o, i];
    for i := 1 to m do
      B[o, i] := A[i];
    result := IntPower(-1, o + 1);
  end;
  for i := 1 to m do
  begin
    S := 0;
    for j := 1 to m do
      S := S + abs(B[i, j]);
    if S = 0 then
      o := 0;
  end;
  if o <> 0 then
    for i := 1 to m do
    begin
      S := 0;
      for j := 1 to m do
        S := S + abs(B[j, i]);
      if S = 0 then
        o := 0;
    end;
  if o = 0 then
    result := 0
  else
  begin
    while m > 1 do
    begin
      k := B[1, 1];
      if k = 0 then
      begin
        o := 0;
        k := 1;
      end;
      for i := 1 to m do
      begin
        B[1, i] := B[1, i] / k;
      end;
      result := result * k;
      for i := 1 to m do
        for j := 1 to m do
          B2[i, j] := B[i + 1, j + 1] - B[i + 1, 1] * B[1, j + 1];
      for i := 1 to m do
        for j := 1 to m do
          B[i, j] := B2[i, j];
      m := m - 1;
    end;
    result := result * B[1, 1];
    if o = 0 then
      result := 0;
  end;
end;

function Back(A: Tm; n: integer): Tm;
var
  i, j, k, l, p, q: integer;
  B, C: Tm;
  detA: Float;
begin
  SetLength(A, n + 1, n + 1);
  SetLength(result, n + 1, n + 1);
  SetLength(B, n + 1, n + 1);
  SetLength(C, n + 1, n + 1);
  detA := det(A, n);
  for i := 1 to n do
  begin
    for j := 1 to n do
    begin
      p := 1;
      q := 1;
      for k := 1 to (n - 1) do
      begin
        if q > n then
          q := q - n;
        if q = j then
          inc(q);
        if q > n then
          q := q - n;
        for l := 1 to (n - 1) do
        begin
          if p > n then
            p := p - n;
          if p = i then
            inc(p);
          if p > n then
            p := p - n;
          B[k, l] := A[q, p];
          inc(p);
        end;
        inc(q);
      end;
      B := tr(B, n - 1, n - 1);
      C[i, j] := det(B, n - 1);
    end;
  end;
  for i := 1 to n do
    for j := 1 to n do
      result[i, j] := (IntPower(-1, i + j) * C[i, j]) / detA;
  if n = 1 then
    result[1, 1] := 1 / A[1, 1];
end;

end.
