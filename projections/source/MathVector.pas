//***************************************************************************
//*    +----------------------------------------------------------------+
//*    |        ��������, ������������� 0800, ����������� 0830          |
//*    |     ----------------------------------------------------       |
//*    | �������� �������� ���������������� ����������.                 |
//*    | ������ � 2009 ����. ��� ������������� � ����������� ���������� |
//*    | ��������� "Copyright 2009 ��������."                           |
//*    +----------------------------------------------------------------+
//*
//*  Project:   ���
//*
//*  File Name: MathVector.pas
//*
//*  Author:    ������ �. �.
//*
//*  Decription: ���������� ��������� ����������
//*
//*
//*  $Revision: 13 $
//*
//*  $Date:: 2009-07-17 12:19:14#$
//*
//*  ������� ���������
//*  �����          ����       ���       �������� ���������
//*  ------------  --------  -------  ---------------------------------
//*  ������ �. �.  01.04.09           ������ ������ �����
//*
//*
//***************************************************************************

unit MathVector;

interface

uses
  SysUtils, Math, MathMatrix;

type
  Float = real;

  vector = record
    X: Float;
    Y: Float;
  end;

function Module(X: vector): Float;
function VecMultiply(X1, X2: vector): vector;
function ScalMultiply(X1, X2: vector): Float;
function CosAngle(X1, X2: vector): Float;
function VecLength(X1, X2: vector): Float;
function VecSumm(X1, X2: vector): vector;
function Angle(F: vector): real;

implementation

function Module(X: vector): Float;
begin
  result := sqrt(sqr(X.X) + sqr(X.Y));
end;

function VecMultiply(X1, X2: vector): vector;
var
  A, B: Tm;
begin
  SetLength(A, 4, 4);
  SetLength(B, 4, 4);
  A[1, 1] := 1;
  A[1, 2] := 0;
  A[1, 3] := 0;
  A[2, 1] := X1.X;
  A[2, 2] := X1.Y;
  A[2, 3] := 0;
  A[3, 1] := X2.X;
  A[3, 2] := X2.Y;
  A[3, 3] := 0;
  B[1, 1] := 0;
  B[1, 2] := 1;
  B[1, 3] := 0;
  B[2, 1] := X1.X;
  B[2, 2] := X1.Y;
  B[2, 3] := 0;
  B[3, 1] := X2.X;
  B[3, 2] := X2.Y;
  B[3, 3] := 0;
  result.X := det(A, 3);
  result.Y := det(B, 3);
end;

function ScalMultiply(X1, X2: vector): Float;
begin
  result := (X1.X * X2.X) + (X1.Y * X2.Y);
end;

function CosAngle(X1, X2: vector): Float;
begin
  if (Module(X1) = 0) or (Module(X2) = 0) then
    result := 0
  else
    result := ScalMultiply(X1, X2) / (Module(X1) * Module(X2));
  if result > 1 then
    result := 1;
  if result < -1 then
    result := -1;
end;

function VecLength(X1, X2: vector): Float;
begin
  result := sqrt(sqr(X2.X - X1.X) + sqr(X2.Y - X1.Y));
end;

function VecSumm(X1, X2: vector): vector;
var
  A, B: Tm;
begin
  SetLength(A, 3, 2);
  SetLength(B, 3, 2);
  A[1, 1] := X1.X;
  A[2, 1] := X1.Y;
  B[1, 1] := X2.X;
  B[2, 1] := X2.Y;
  A := Summ(A, B, 2, 1);
  result.X := A[1, 1];
  result.Y := A[2, 1];
end;

function Angle(F: vector): real;
var
  cosFi: Float;
begin
  if abs(F.X) + abs(F.Y) = 0 then
    result := 2 * pi
  else
  begin
    cosFi := abs(F.Y) / sqrt(sqr(F.X) + sqr(F.Y));
    if (F.X > 0) and (F.Y > 0) then
      result := arcCos(cosFi);
    if (cosFi = 1) and (F.Y > 0) then
      result := 0;
    if (F.X > 0) and (F.Y < 0) then
      result := pi - arcCos(cosFi);
    if (F.X < 0) and (F.Y < 0) then
      result := pi + arcCos(cosFi);
    if (cosFi = 1) and (F.Y < 0) then
      result := pi;
    if (F.X < 0) and (F.Y > 0) then
      result := 2 * pi - arcCos(cosFi);
    if cosFi = 0 then
    begin
      if F.X > 0 then
        result := pi / 2;
      if F.X < 0 then
        result := 3 * pi / 2;
    end;
  end;
  if result >= 2 * pi then
    result := result - 2 * pi;
end;

end.
